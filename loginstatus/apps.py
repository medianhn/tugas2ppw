from django.apps import AppConfig


class LoginStatusConfig(AppConfig):
    name = 'loginstatus'
